# La fiabilité

`Undo = défaire après une panne pour trouver l'état stable`

`Redo = refaire après une panne pour trouver un état stable`

`Les logs sont dans la RAM, si on ne demande pas à le mettre sur HDD et bien il reste tout le temps en RAM`

`Pour mettre les logs dans le HDD -> flush logs`

`Les logs aident à la reprise sur panne mais on es pas sûr de pouvoir les récuperer`

`Dans undo : U(T, a, 8) => Update transaction T variable a valeur avant la transaction = 8`

`Dans redo : U(T, a, 16) => Update transaction T variable a valeur après la transaction = 16`

`output fait passer la valeur d'une variable sur le disque dur et pas juste dans la RAM`

`On s'en fiche de la vitesse lors d'une panne de BD, on prend le temps de bien récupérer les vraies valeurs, tant pis si c'est pas optimisé`

`En mode undo, toutes les variables sont transférées sur le HDD avant le commit`

`En mode redo on refait les étapes dans l'ordre des logs (en undo c'est le sens inverse)`

`Le redo vu que les output (mise sur HDD) se fait après le commit, ne permet pas forcément de savoir si lorsqu'il y a eu une panne si les variables ont déjà été mises dans le HDD ou non`

`En undo/redo (en même temps hein) on refait toutes les transactions dont on voit le commit #Redo et on défait toutes celles qui n'ont pas de commit #Undo`

## Exercice 1

Pour les trois colonnes, les actions représentent ce qu'un utilisateur effectue vis-à-vis de la base de données (chaque ligne) et la colonne journal représente les logs qui vont être écrit pour chaque action.

S'il y a une panne n'importe quand avant le flush logs alors dans tous les cas on ne fait rien car il n'y aura pas de logs

### Undo

On ne peut commencer à faire quelque chose seulement après la ligne 12, dans ce cas là, on refera les opérations dans l'ordre inverse car la transaction a été commitée.
Donc on fera b = 5 et a = 8 et on dira à l'utilisateur que sa transaction n'a pas pu être faites.

### Redo

On ne peut commencer à faire quelque chose qu'après la ligne 9.
Ici on pourra voir que la transaction a bien été commitée, mais on ne sait cependant pas si les variables ont été restockées dans le disque dur, de ce fait on refera a = 16 et b = 10

### Undo/Redo

On ne peut commencer à faire quelque chose qu'après la ligne 8.
On verra que la transaction ne comporte pas de commit donc on fera de l'undo et on remettra b = 5 et a = 8.
Après l'autre flush log à la ligne 11 on verra qu'il y a eu le commit, donc on fera a = 16 et b = 10.

## Exercice 2

`Code : 0 = objet (resource), A = after B = before`

B(T1)
B(T2)
U(T2, O1, B1, A1)
I(T1, O2, A2),
B(T3)
C(T1),
B(T4)
U(T3, O2, B3, A3)
U(T4, O3, B4, A4)
C(T4),
B(T5)
U(T3, O3, B5, A5)
U(T5, O4, B6, A6)
D(T3, O5, B7)
A(T3)
C(T5)
I(T2, O6, A8)

1. Décrivez pas à pas la procédure de redémarrage à chaud. (#Undo/Redo)

### Analyse des transactions

Il faut défaire T2 et T3 (elles n'ont pas eu de commit)
Il faut refaire T1, T4 et T5.

### Les undos

B(T2)
D(T2, 06, A8)
B(T3)
I(T3, 05, B7)
U(T3, 03, A5, B5)
U(T3, 02, A3, B3)
U(T2, 01, A1, B1)
C(T2)
C(T3)

### Les redos

B(T1)
B(T4)
B(T5)
I(T1, 02, A2)
U(T4, 03, B4, A4)
U(T5, 04, B6, A6)
C(T1)
C(T4)
C(T5)

2. Réécrivez ce fichier de journal en considérant que nous sommes en fait en mode Redo. `En gros je supprime les before, B`

_Là je refais le journal donc que la transaction soit finie ou non je note_

A(T2) `On met ça seulement en redo comme ça on en es sûr que ça pose pas de soucis`
A(T3) `On met ça seulement en redo comme ça on en es sûr que ça pose pas de soucis`
B(T1)
B(T2)
U(T2, O1, A1)
I(T1, O2, A2),
B(T3)
C(T1),
B(T4)
U(T3, O2, A3)
U(T4, O3, A4)
C(T4),
B(T5)
U(T3, O3, A5)
U(T5, O4, A6)
D(T3, O5)
A(T3)
C(T5)
I(T2, O6, A8)

3. Décrivez pas à pas la procédure de redémarrage à chaud en mode Redo.

A(T2) `On met ça seulement en redo comme ça on en es sûr que ça pose pas de soucis`
A(T3) `On met ça seulement en redo comme ça on en es sûr que ça pose pas de soucis`
B(T1)
I(T1, O2, A2),
C(T1),
B(T4)
U(T4, O3, A4)
C(T4),
B(T5)
U(T5, O4, A6)
C(T5)

4. Réécrivez ce fichier de journal en considérant que nous sommes en fait en mode Undo.

B(T1)
B(T2)
U(T2, O1, B1)
I(T1, O2),
B(T3)
C(T1),
B(T4)
U(T3, O2, B3)
U(T4, O3, B4)
C(T4),
B(T5)
U(T3, O3, B5)
U(T5, O4, B6)
D(T3, O5, B7)
A(T3)
C(T5)
I(T2, O6)

5. Décrivez pas à pas la procédure de redémarrage à chaud en mode Undo.

D(T5, O4, B6)
D(T4, O3, B4)
I(T1, O2)

## Exercice 3

a=30, b=15, c=40, d=20, e=12 et f=35

B(T1)
U(T1, d, 21)
C(T1)
B(T2)
U(T2, b, 12)
B(T4)
U(T4, e, 15)
B(T3)
U(T3, a, 35)
U(T4, f, 20)
C(T4)
U(T2, d, 25)

1. Quel protocole de journalisation a été utilisé pour générer ce journal ?

Il s'agit de redo car avec redo on ne garde que la valeur modifié lors d'un update par exemple et non pas la valeur avant l'action.
Or on peut constater que la toute première opération de modification U(T1, d, 21) ne fais pas référence à la valeure initiale de la ressource d.

2. Une panne survient après la dernière opération ci-dessus. Décrivez les opérations de
reprise sur panne.

B(T1)
U(T1, d, 21)
C(T1)
B(T4)
U(T4, e, 15)
U(T4, f, 20)
C(T4)

3. Si un point de contrôle avait débuté juste après le début de la transaction T2, que pourriez-
vous en déduire sur le type de point de contrôle utilisé ? Où pourrait se trouver l’instruction
de fin de point de contrôle ?

`Le checkpoint, bah ça sert à faire un checkpoint quoi : En gros pour ne pas avoir à relire tout le journal de log on établi un checkpoint qui sert à figer l'état de la base de donnée en faisant des outputs (stockant dans le HDD de la base de donnée) des transactions commitées. Pour les transactions qui n'ont pas été commité avant le point de checkpoint, il faut les défaires et les refaire après le checkpoint.`

`Checkpoint quiescient et non quiescient : non quiescent à une notion de temporalité, faire le checkpoint prend du temps d'ou la notion de strart stop`

Le checkpoint aurait été non quiscent car étant donné qu'il y a encore des transactions après, il va avoir besoin de ne pas 'se mettre en travers de la route'.

1. Réécrivez ce fichier de journal en considérant que nous sommes en fait en mode
Undo/Redo.

a=30, b=15, c=40, d=20, e=12 et f=35

B(T1)
U(T1, d, 20, 21)
C(T1)
B(T2)
U(T2, b, 15, 12)
B(T4)
U(T4, e, 12, 15)
B(T3)
U(T3, a, 30, 35)
U(T4, f, 35, 20)
C(T4)
U(T2, d, 20, 25)

5. Décrivez pas à pas la procédure de redémarrage à chaud en mode Undo/Redo.

`C'est toujours d'abord défaire puis refaire`

A défaire : T2, T3
A refaire : T1, T4

UNDO

B(T3)
B(T2)
U(T2, d, 20)
U(T3, a, 30)
U(T2, b, 15)
C(T2)
C(T3)

REDO

B(T1)
B(T4)
U(T1, d, 21)
C(T1)
U(T4, e, 15)
U(T4, f, 20)
C(T4)

## Exercice 4

B(T1)
U(T1, d, 21)
C(T1)
B(T4)
U(T4, e, 15)
START CKPT(T4)
U(T4, f, 20)
C(T4)
B(T2)
U(T2, b, 12)
B(T3)
U(T3, a, 35)
U(T2, d, 25)

1. Que signifie le T4 en paramètre du démarrage sur Check Point ?

Cela signifie que la transaction 4 a été commencé avant de commencer à faire le checkpoint et que du coup malgré le checkpoint, il faudrait refaire cette transaction dans tous les cas dans son intégralité.

2. Une panne survient à la fin de cette séquence. Décrivez la procédure de reprise.

Comme pour l'exercice précédent, nous sommes sur du redo.

Et comme dit dans la question suivante rien est changé.

3. Comment avez-vous tenu compte du Check Point démarré ?

`Ici ça ne sert à rien de prendre en compte le checkpoint étant donné qu'il ne comporte pas de end donc il sert à rien là`

## Exercice 5

U(T1, d, 21)
C(T1)
B(T4)
U(T4, e, 15)
START CKPT(T4)
U(T4, f, 20)
C(T4)
B(T2)
U(T2, b, 12)
B(T3)
U(T3, a, 35)
U(T2, d, 25)
END CKPT

_Une panne survient à la fin de cette séquence. Décrivez la procédure de reprise._

Comme précédemment on es sur du redo.

Donc ici grâce au checkpoint je ne m'occupe pas des transactions qu'il y a avant le checkpoint sauf celle entre parenthèse, soit la transaction 4.

B(T4)
U(T4, e, 15)
U(T4, f, 20)
C(T4)
B(T2)
U(T2, b, 12)
B(T3)
U(T3, a, 35)
U(T2, d, 25)

## Exercice 6

_Voici un fichier de log en Undo/Redo :_

B(T1)
B(T2)
B(T3)
I(T1,O1,A1)
D(T2, O2, R2)
B(T4)
U(T4, O3, B3, A3)
C(T2)
START CKPT(T1, T3, T4)
U(T1, O4, B4, A4)
END
CKPT
B(T5)
B(T6)
U(T5, O5, B5, A5)
A(T3)
START CKPT(T1, T4, T5, T6)
B(T7)
A(T4)
U(T7, O6, B6, A6)
U(T6, O3, B7, A7)
B(T8)
A(T7)
panne

1. Décrivez le redémarrage à chaud.

Ici, nous pouvons constater qu'il y a un checkpoint non quiescent, puis un checkpoint quiescent puis un non quiescent
qui ne possède pas de fin.
De ce fait le dernier checkpoint ne peut être établi. Ainsi, le dernier checkpoint valable en date et le checkpoint quiescent. Ici aucune transaction n'a pu être commitée, nous allons donc faire du undo.
De ce fait il faut faire :

B(T5)
B(T6)
U(T6, 03, B7)
U(T5, 05, B5)
C(T5)
C(T6)

2. Imaginons maintenant que survient une panne de dispositif impliquant les objets O1, O2
et O3. Décrivez le redémarrage à froid.

B(T1)
B(T2)
B(T3)
B(T5)
B(T6)
I(T1,O1,A1)
D(T2, O2, R2)
U(T6, 03, B7)
U(T5, 05, B5)
C(T1)
C(T2)
C(T3)
C(T5)
C(T6)

## Exercice 7

_Une journalisation de type Undo a produit le fichier de log suivant :_

B(TS)
U(TS, a, 60)
C(TS)
B(TT)
U(TT, a, 61)
B(TU)
U(TU, a, 62)
U(TT, c, 30)
B(TV)
U(TU, d, 40)
U(TV, f, 70)
C(TU)

1. Quel sera l’effet sur les données si une panne survient à la fin ?

Ici, seules les transactions S et U sont commitées. Etant donné que la journalisation est en Undo.
Il va donc falloir défaire les transactions T et V.
Le problèmes est que les transactions T et V font intervenir des ressources qui sont elles-mêmes utilisées par les transactions S et U.

B(TS)
B(TU)
U(TS, a, 60)
U(TU, a, 62)
U(TU, d, 40)
C(TS)
C(TU)

Ici le problème vu que nous sommes en undo c'est qu'on dit que la ressource a sa valeur est à 60 puis juste après on dit
que sa valeur est à 62. De ce fait nous avons une incohérence.

2. Précisez quel est le problème et dites comment nous pourrions y remédier.

Il aurait fallu faire du undo/redo plutôt que simplement de l'undo