# Exercice 1

_Un client indique sa ville de départ, sa ville d’arrivée et l’heure désirée pour le vol qu’il souhaite réserver. Les informations concernant le(s) vol(s) correspondant(s) sont recherchées dans la base de données. Elles sont localisées dans les blocs A et B du disque sur lequel est stockée la base de données._

> read (ville depart, ville arrivée, here) A ; read (ville depart, ville arrivée, here) B

_Les vols possibles sont présentés au client qui en choisit un dont les données, y compris le nombre actuel de réservations, se trouvent stockées dans le bloc B. Une réservation pour ce client sur ce vol est effectuée._

> read (vols disponibles, nombre de reservations) B ; write (reservation cliente) B

_Le client choisit alors un siège pour le vol. Les données qui correspondent à ces informations sont localisées dans le bloc C._

> read (sièges disponibles) C ; write (siège choisi) C

_Le client donne son numéro de carte de crédit et une facture est ajoutée à la liste des factures associées à ce vol. Elle est stockée dans le D._

> write (carte de crédit, facture) D

_L’adresse de courriel du client est insérée dans une autre liste, stockée dans le bloc E, afin de permettre la confirmation de la réservation._

> write (adresse mail) E

# Exercice 2

1. r1(x), r1(y), w1(x), w1(y), c1, c2

> RAS (car on suppose que la transaction 2 à été ouverte mais on a rien fait)

2. r1(x), r2(z), w1(x), w2(z), c1, c2

> RAS

3. r1(x), w1(x), r2(x), w2(y), a1, c2

> Dirty Read

4. r1(x), w1(x), r2(y), w2(y), a1, c2

> RAS

5. r1(x), r2(x), r2(y), w2(y), r1(z), a1, c2

> RAS

6. r1(x), r2(x), w2(x), w1(x), c1, c2

> Update loss

7. r1(x), r2(x), w2(x), r1(x), c1, c2

> Inconsistant read

8. r1(x), w1(x), r2(y), w2(x), c1, c2

> RAS (on efface juste ce que l'autre a fait)

# Exercice 3

_Parmi les séquences suivantes (toutes sont automatiquement validées par un commit implicite à la fin), lesquelles sont vue-sérialisables ? Pour celles qui sont vue-sérialisables, donnez toutes leurs vue-sérialisations valides._

`Read From = read précédé d'un write (wr) sur une même ressource (pas forcément collés, juste précédés)`
`Final Write = La dernière écriture pour chaque ressource`
`Sérialisé = reordonner (sans mélanger ce qu'on fait les personnes) pour que ça marche juste on reorganise en changeant l'ordre des transactions (le numéro) après on recalcule les RF et les FW et si se sont les mêmes alors la séquence est vue sérialisable et ça peut être exécuté sans problèmes`

*Pourquoi ? Parce que ce genre de séquence ça sert si plusieurs personnes travaillent sur des mêmes ressources. Ils font leurs choses blabla, puis au moment où elles commits le SGBD va faire ce qu'on fait là et si c'est une séquence non sérialisable et bien il ne va rien faire et dire aux utilisateurs de réessayer plus tard (pour essayer de tomber sur une autre séquence)*

*Technique regarder l'ordre des readfroms de la séquence initiale triée, rien que grâce à eux je peux tirer des règles de qui doit être avant qui*

*En réalité il ne suffit que d'une seule vue sérialisée mais là on les fait toutes pour s'entraîner*

1. w0(x), r2(x), r1(x), w2(x), w2(z)

RF = [ w0(x), r1(x) ]
FW = w2(x), w2(z)

w0(x), r1(x), r2(x), w2(x), w2(z) -> 012

RF = [ w0(x), r1(x) ]
FW = w2(x), w2(z)

w0(x), r2(x), w2(x), w2(z), r1(x) -> 021

RF = [ w0(x), r2(x) ]

> STOP : On a déjà quelque chose de différent

r1(x), w0(x), r2(x), w2(x), w2(z) -> 102

RF = [ w0(x), r2(x) ]

> STOP : On a déjà quelque chose de différent

r1(x), r2(x), w2(x), w2(z), w0(x) -> 120

> On ne peut pas faire de Read From si tous les writes finisses la séquence, ainsi la combinaison 120 ne fonctionne pas

r2(x), w2(x), w2(z), r1(x), w0(x) -> 210

RF = [ w2(x), r1(x)]

> STOP : On a déjà quelque chose de différent

> Pour les combinaisons 210 et 201 la transaction 2 étant devant et comportant au moins un write et au moins une des deux autres transactions comportant un read, on obtiendra forcément des Read Froms avec la transaction 2, or il n'y en a pas dans la transaction initiale triée. --> Du coup la séquence est vue-sérialisable d'ordre 012

2. w0(x), r1(x), r2(x), w2(x), w2(z) -> déja en ordre 012

RF = [ w0(x), r1(x) ], [ w0(x), r2(x) ]
FW = w2(x), w2(z)

> La seule solution pour retomber sur les mêmes Read From est d'avoir la transaction 0 en premier. De plus en mettant la transaction 2 avant la transaction 1 on implique forcément d'autres Read From. Du coup il n'y a pas d'autres possibilités. La séquence est vue-sérialisable sur l'ordre 012.

3. w0(x), r1(x), w1(x), r2(x), w1(z)

RF = [ w0(x), r1(x) ], [ w0(x), r2(x) ], [ w1(x), r2(x) ]
FW = w1(x), w1(z)

> Pour avoir des vues-sérialisables ici, d'après les Read Fromw, il faut forcément que la transaction 0 soit en premier (pour ne pas avoir de FW w0 à cause de lui) de plus il faut forcément que la transaction 1 soit avant la transaction 2 d'après les RF. Aisni le seul ordre potentiellement vues-sérialisable est 012.

w0(x), r1(x), w1(x), w1(z), r2(x)

RF = [ w0(x), r1(x) ], [ w0(x), r2(x) ], [ w1(x), r2(x)  ]
FW = w1(x), w1(z)

> La séquence d'ordre 012 est vue-sérialisable

4. w0(x), r1(x), w1(x), w1(z), r2(x)

RF = [ w0(x), r1(x) ], [ w0(x), r2(x) ], [ w1(x), r2(x) ]
FW =  w1(x), w1(z)

> Ici les Read From impliquent que la transaction 0 arrive avant la transaction 1, que la transaction 0 arrive avant la transaction 2 et que la transaction 1 arrive avant la transaction 2. Ainsi le seul ordre possible est 012. Cependant cette séquence étant déjà ordonnée dans l'ordre 012, elle est vue sérialisable d'ordre 012.

5. r1(x), r2(x), w2(x), w1(x)

RF = ///
FW = w2(x), w1(x)

> Ici, nous n'avons que les Final Write. Ainsi, pour retomber sur les mêmes Read From et Final Writes, nous devons obligatoirement avoir les writes à la fin ce qui est impossible dès lors que nous ordonnons les transactions. Cette séquence n'est donc pas vue-sérialisable.

6. r1(x), r1(y), r2(z), r2(y), w2(y), w2(z), r1(z)

RF = w2(z), r1(z)
FW = w2(y), w2(z)

> L'unique Read From nous apprend que la transaction 2 doit se passer avant la transaction 1, impliquant l'ordre 21

r2(z), r2(y), w2(y), w2(z), r1(x), r1(y), r1(z)

RF = [ w2(y), r1(y) ], [ w2(z), r1(z) ]

> STOP, nous avons déjà un élément de plus dans les Read From donc ce n'est pas la peine de continuer. La séquence n'est pas vue-sérialisable

7. r0(x), r0(y), w0(x), r1(y), r1(x), w1(y), r2(x), r2(y), r2(z), w2(z)

RF = [ w0(x), r1(x) ], [ w1(y), r2(y) ]
FW = w0(x), w1(y), w2(z)

> Ici, les Read Froms impliquent que la transation 0 arrive avant la 1 et que le transaction 1 arrive avant la 2. Ainsi, le seul prdre possible est 012. Cependant, cette séquence étant déjà ordonnée selon l'ordre 012, nous pouvons en conclure qu'elle est vue-sérialisable sur l'ordre 012.

# Exercice 4

`Un conflit sérialisation n'est pas un vrai conflit, le but est d'éviter que plusieurs personnes travaillant sur une base de données ne se gène pas, comme pour la vue sérialisation, il s'agit d'un algorithme pour éviter les conflits.`

`Cependant, cet algorithme est plus simple.`

`On va chercher à voir les conflits WW, RW et WR sur une`

`On va toujours chercher le premier élément (WW, et RW) pour une transaction (s'il y en a plusieurs osef, on prend celui qui arrive en premier)`

`Il faut que l'on tombe sur les mêmes conflits pour que la séquence puisse être exécutée sans qu'il y ai de problèmes`

`C'est sur une même ressource avec des transactions différentes, comme pour la vue sérialisable`

`Par contre ça ne permet du coup pas de savoir si c'est vue serialisable`

`Et une fois de plus, il suffit qu'un seul ordre fonctionne`

1. r1(x), r2(x), w2(x), w1(x)

WW : w2(x) w1(x)
RW : r1(x) w2(x)
WR : ////

> Ici le WW impliquent que la transaction 2 soit avant la transaction 1 mais les RW impliquent l'inverse, donc ça n'est pas conflit sérialisable

2. r1(x), r1(y), r2(z), r2(y), w2(y), w2(z), r1(z)

WW : ///
RW : r1(y) w2(y)
WR : w2(z) r1(z)

> Ici la transaction 2 doit forcément être avant la transaction 1 (à cause du WR car le RM ne dit rien de particulier). Donc elle n'est pas conflit sérialisable, `ce qui ne nous permet pas de savoir si elle est vue sérialisable`

3. r1(x), w1(x), r2(z), r1(y), w1(y), r2(x), w2(x), w2(z)

WW : w1(x) w2(x)
RW :  r1(x) w2(x)
WR : [ w1(x) r2(x) ]

> La transaction 1 doit être avant la transaction 2

r1(x), w1(x), r1(y), w1(y), r2(z), r2(x), w2(x), w2(z)

WW : w1(x) w2(x)
RW : r1(x) w2(x)
WR : [ w1(x) r2(x) ]

> OK

4. r1(x), w1(x), w3(x), r2(y), r3(y), w3(y), w1(y), r2(x)

WW : [w1(x), w3(x)] [w3(y), w1(y)]
RW : [r1(x), w3(x)] [r2(y), w3(y)]
WR : [w1(x), r2(x)]

> Impossible car le WW dit que la transaction 1 soit avant la 3 et que la 3 sois avant la 1

5. r1(x), r2(x), w2(x), r3(x), r4(z), w1(x), w3(y), w3(x), w1(y), w5(x), w1(z), w5(y), r5(z)

WW : [ w2(x), w1(x) ], [ w3(y), w1(y) ], [ w1(x), w3(x) ], [ w1(y), w5(y) ]
RW : [ r1(x), w2(x) ]

> STOP -> transaction 2 avant la 1, transaction 1 avant la 2 -> impossible #Deadlock

6. r1(x), r3(y), w1(y), w4(x), w1(t), w5(x), r2(z), r3(z), w2(z), w5(z), r4(t), r5(t)

WW : [ w4(x), w5(x) ], [ w2(z), w5(z) ]
RW : [ r1(x), w4(x) ], [ r3(y), w1(y) ], [ r2(z), w2(z) ], [ r3(z), w2(z) ]
WR : [ w1(t), r4(t) ]

> 4 avant 5 ; 2 avant 5 ; 1 avant 4 ; 3 avant 1 ; 3 avant 2 ; 1 avant 4
> => 32145 ; 31245 ; 31425

32145 => r3(y), r3(z), r2(z), w2(z), r1(x), w1(y), w1(t), w4(x), r4(t), w5(x), w5(z), r5(t)

WW : [ w2(z), w5(z) ], [  w4(x), w5(x) ]
RW : [ r3(y), w1(y) ], [ r3(z), w2(z) ], [ r2(z), w2(z) ], [ r1(x), w4(x) ]
WR : [ w1(t), r4(t) ], [ w1(t), r5(t) ]

31245 => r3(y), r3(z), r1(x), w1(y), w1(t), r2(z), w2(z),  w4(x), r4(t), w5(x), w5(z), r5(t)

WW : [ w2(z), w5(z) ], [  w4(x), w5(x) ]
RW : [ r3(y), w1(y) ], [ r3(z), w2(z) ], [ r1(x), w4(x) ], [ r2(z), w2(z) ]
WR : [ w1(t), r4(t) ],  [ w1(t), r5(t) ]

31425 => r3(y), r3(z), r1(x), w1(y), w1(t), w4(x), r4(t), r2(z), w2(z), w5(x), w5(z), r5(t)

WW : [  w4(x), w5(x) ], [ w2(z), w5(z) ]
RW : [ r3(y), w1(y) ], [ r3(z), w2(z) ], [ r1(x), w4(x) ], [ r2(z), w2(z) ]
WR : [ w1(t), r4(t) ], [ w1(t), r5(t) ]

> C'est conflit sérialisable sur les ordres 32145 ; 31245 ; 31425

# Exercice 5

_Vous utiliserez les règles suivantes pour placer des verrous :_
_Tous les verrous sont exclusifs._
_Tous les verrous sont placés juste devant l’opération (lecture ou écriture) qui nécessite le verrouillage._
_Tous les verrous sont libérés juste après la fin de la transaction._

1. w0(x), r2(x), r1(x), w2(x), w2(z)

l0(x), w0(x), u0(x), l2(x), r2(x), l1(x) -> `IMPOSSIBLE`

2. w0(x), r1(x), r2(x), w2(x), w2(z)

l0(x), w0(x), u0(x), l1(x), r1(x), u1(x), l2(x), r2(x), w2(x), w2(z), u2(x)

3. w0(x), r1(x), w1(x), r2(x), w1(z)

l0(x), w0(x), u2(x), l1(x), r1(x), w1(x), u1(x), l2(x), r2(x), u2(x), l1(z), w1(z), u1(z)

1. w0(x), r1(x), w1(x), w1(z), r2(x)

l0(x), w0(x), u0(x), l1(x), r1(x), w1(x), l1(z), w1(z), u1(x), u1(z), l2(x), r2(x), u2(x)

5. r1(x), r2(x), w2(x), w1(x)

l1(x), r1(x), -> `IMPOSSIBLE`

6. r1(x), r1(y), r2(z), r2(y), w2(y), w2(z), r1(z)

l1(x), r1(x), l1(y), r1(y), l2(z), r2(z), l2(y), r2(y), w2(y), w2(z), u2(z), u2(y), r1(z), u1(x), u1(z), u1(y)

7. r0(x), r0(y), w0(x), r1(y), r1(x), w1(y), r2(x), r2(y), r2(z), w2(z)

l0(x), r0(x), l0(y), r0(y), w0(x), u0(x), u0(y), l1(y), r1(y), l1(x), r1(x), w1(y), u1(y), u1(x), l2(x), r2(x), l2(y), r2(y), l2(z), r2(z), w2(z), u2(x), u2(y), u2(z)

# Exercice 6

_Vous utiliserez maintenant les règles suivantes pour placer des verrous :_
_Tous les verrous partagés sont placés juste devant l’opération de lecture qui nécessite le verrouillage._
_Tous les verrous exclusifs sont placés juste devant l’opération d’écriture qui nécessite le verrouillage._
_Tous les verrous sont libérés :_
_soit dès qu’une autre transaction a besoin de la ressource mais seulement si la transaction qui l’a déjà verrouillée n’en a plus besoin,_
_soit juste après la fin de la transaction._

1. w0(x), r2(x), r1(x), w2(x), w2(z)

x_l0(x), w0(x), u0(x), s_l2(x), r2(x), r1(x), x_l2(x), w2(x), x_l2(z), w2(z), u2(x), u2(z)

2. w0(x), r1(x), r2(x), w2(x), w2(z)

x_l0(x), w0(x), u0(x), s_l1(x), r1(x), r2(x), u1(x), x_l2(x), w2(x), x_l2(z), w2(z), u2(x), u2(z)

3. w0(x), r1(x), w1(x), r2(x), w1(z)

x_l0(x), w0(x), u0(x), s_l1(x), r1(x), u1(x), x_l1(x), w1(x), u1(x), s_l2(x), r2(x), u2(x), x_l1(x), w1(z), u1(x)

1. w0(x), r1(x), w1(x), w1(z), r2(x)

x_l0(x), w0(x), u0(x), s_l1(x), r1(x), w1(x), x_l1(z), w1(z), u1(x), u1(z), s_l2(x), r2(x), u2(x)

5. r1(x), r2(x), w2(x), w1(x)

s_l1(x), r1(x), r2(x), u1(x), x_l2(x), w2(x), u2(x), x_l1(x), w1(x), u1(x)

6. r1(x), r1(y), r2(z), r2(y), w2(y), w2(z), r1(z)

s_l1(x), r1(x), s_l1(y), r1(y), s_l2(z), r2(z), s_l2(y), r2(y), w2(y), x_l2(z), w2(z), u2(z), u2(y), s_l1(z), r1(z), u1(z), u1(x), u1(y)

7. r0(x), r0(y), w0(x), r1(y), r1(x), w1(y), r2(x), r2(y), r2(z), w2(z)

s_l0(x), r0(x), s_l0(y), r0(y), u0(x), x_l0(x), w0(x), u0(x), u0(y), s_l1(y), r1(y), s_l1(x), r1(x), u1(y), x_l1(y), w1(y), u1(y), u1(x), s_l2(x), r2(x), s_l2(y), r2(y), s_l2(z), r2(z), u2(z), x_l2(z), w2(z), u2(z), u2(x), u2(y)

# Exercice 7