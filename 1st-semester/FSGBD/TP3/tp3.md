# Systèmes de contrôle d'accès

## Prise de notes

Le but de l'indexation c'est de faire référence à des données dans la table.
Le but est de parcourir la table le plus vite possible, il faut ordonner les index

Sois on peut indexer toutes les données, bêtement -> chaque entrées dans la BD est indexée dans la table des index.
Sois on en index qu'une certaine partie comme dans l'exemple ci-dessous

![indexation](/1st-semester/FSGBD/TP3/assets/indexation.png)

Et dans l'exemple ci-dessus, si on veut l'élément 20 on sait qu'on a indexé 10 et 40 du coup on sait que 20 est entre
10 et 40 du coup on va à l'index de 10 et on descend jusqu'à avoir 20.

En vrai dans le cours c'est faux, c'est pas un arbre binaire, c'est un arbre balancé

L'ordre est pré-établi et chaque feuilles peut contenir 2 * ordre feuilles max (sauf la racine qui peut `tout le temps` avoir entre 0 et 2 * ordre)

Pour ajouter une nouvelle feuille à l'arbe balancé, dès qu'une feuille dépasse l'ordre établi, on la split en deux et on fait remonter la valeur intermédiaire à l'étage du dessus

Dès qu'on veut ajouter un élement à l'arbre, c'est forcément au niveau de ses feuilles les plus basses, après ça peut remonter à cause des pivots

Cet algorithme ne peut donner qu'une seule solution lors de la création de l'arbre

Si par hasard le pivot donne un nombre impair ou autre qui pourrait aller dans deux feuilles, on choisit arbitrairement si on met à gauche ou à droite mais on fera ça tout le long

On a toujours un fil de plus que le nombre de valeur dans le fils

La complexité de cet algorithme est grande mais on s'en fiche parce que dans une base de données on passe la plupart du temps à chercher des infos dans le BD plutôt que d'insérer
Par contre la complexité de la recherche est très rapide

Une feuille qui n'est pas à la fin ne peux pas n'avoir qu'un seul fils, faut équilibrer

`Les diapos à partir de la 29 expliquent bien ça`

## Exercice 1

(13n)/30 = n/3 + n/10 -> nombre de blocs pour n tuples + nombre de blocas pour n paires
(11n)/30 = n/30 + (n/3)/10

## Exercice de cours
9	87	13	50	31	88	36	1	97
0	27	100	54	40	6	3	7	77	21

Arbre d'ordre 2 (= entre 2 et 4 valeurs par feuille)

`En exercice, on doit écrire chaque étape d'insertion pour voir la progression de l'arbre, et on souligne la valeur d'insertion`

31
3-9		50-87
0-1 	6-7		13-21-27 	36-40-54-77 	89-97-100

## Exercice

# 1.

1589
1668
1685
1684
1862
1875
1756
1856

1. .

1589

2. .

1589 1668

3. .

1668
1589-1685

4. .

1668-1685
1589-1684

5. .

1668-1685
1589-1684 1862

6. .

1668-1685
1589-1684 1862-1875

7. .

1685
1668 1862
1589-1684 1756-1875

8. .

1685
1668 1862
1589-1684 1756-1856 1875

`Il faut toujours regarder par rapport au nombre au dessus si c'est plus petit ou plus grand`