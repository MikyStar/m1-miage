# Partie 1

- Quelles sont, selon vous, les finalités d'un document de recueil des exigences ?

Le but est d'explicité au maximum les besoins du client et y amener des réponses de manière structurée.
C'est ainsi que les diverses fonctionnalités ainsi que leur spécification détaillées sont exprimées, le tout dans un cadre le moins ambigue possible que ce soit pour le client et le développeur.

- A quoi servent chacune des exigences listées dans ce document ?

Elles servent toutes à définir précisément les attentes du client en termes de fonctionnalités. Ainsi le titre de chacune des exigences peut s'apparenter à une tâche d'ingéniérie à proprement parlé et les explications de ces dernières permettent de définir leur spécification afin de coller le mieux possible avec l'attente du client. Cela permet égallement de définir les limites de chacunes des exigences afin de décrire précisement au client ce que son produit finale pourra faire, sans ambiguité.
Les numéroter permet de donner un ordre chronologique et de facilement faire référence au fonctionnalité que ce soit vis-à-vis des tests

# Partie II. Etude détaillée des exigences

1. ambiguë. Justifiez votre choix.

FCT-41-Demande inhabituelle

> Ici, rien ne stipule ce qui caractérise une demande inhabituelle ni même quelle démarche doit suivre un commerciale pour signaler au service d'achat en quoi elle est inhabituelle

2. difficile à comprendre. Justifiez votre choix.

FCT-5-Référence client

> La réutilisation très fréquente du mot "référence" en modifiant sa signification porte très facilement à confusion et résulte d'une perte d'information.

3. non testable. Indiquer comment la reformuler pour qu'elle le soit.

FCT-105-Reconfiguration

`Tout ce qui est ambigue n'est pas testable, genre avec les trois petits points c'est ambigue`

> Pour que ce soit testable, il faudrait décrire précisément le temps de reconfiguration de chacune des

4. non atomique. Pourquoi cela est-il problématique ? Indiquer comment la reformuler pour qu'elle le soit.

FCT-40-Planning commande

> Il y a plusieurs étapes toutes en une seule donc c'est très difficilement testable. Il faut couper pour faire de l'atomicité

1. qui décrit une solution et non un problème. Pourquoi est-ce problématique ?

FCT-16-Plusieurs tarifs

> Ici on parle directement de la solution sans parlé de pourquoi le client a besoin de plusieurs tarifs. On ne parle de prendre en charge les différents types de commissionnement sans expliquer ce qui est impliqué.

6. pour laquelle plusieurs solutions (implémentations) sont envisageables. Quel(s) intérêt(s) y voyez-vous ?

`TODO`
> Plusieurs implémentations possibles permet une meilleure flexibilité au niveau de la conception car les développeurs peuvent choisir l'implémentation qui sied le mieux au restant du projet