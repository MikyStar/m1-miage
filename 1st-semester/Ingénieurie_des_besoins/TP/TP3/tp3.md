# Partie 1

1. Capacité fonctionnelle

Si je veux payer en ligne, alors je dois pouvoir payer par carte bleue

2. Fiabilité

Si jamais j'ai une panne dans ma BD, je veux être capable de retrouver mes données dans leur bonne état.

3. Facilité d'utilisation

Je veux pouvoir trouver en trois clicks ce dont j'ai besoin.

4. Rendement ou efficacité

Je veux pouvoir chercher un utilisateur dans ma base de données en moins de 3 secondes.

1. Maintenabilité

Je veux pouvoir pouvoir payer en ligne via une plateforme quelconque dans un futur proche sans changer le reste de la solution.

6. Portabilité

Je veux que mon application soit cross-plateforme.

# Partie 2

`Externe = Il faut considérer le code comme une boite noire`
`Interne = Dans le code`
`Tous ces éléments viennent de la page 7 de la norme ISO en pdf`

- NFCT-01-Ergonomie entrepots

utilisabilité -> opérabilité
Interne

- NFCT-02-Droit d'accès

fonctionnalité -> sécurité
Externe

- NFCT-03-Comptabilité internationale

Fiabilité -> conformité
Externe

- NFCT-04-Redémarrage serveur

Fiabilité -> récupération

- NFCT-05-Info-bulles

Utilisabilité -> compréhensivité
Externe

- NFCT-06-Contenu minimal

Fonctionnalité -> Convenabilité
Il faut préciser ce que sont les 80%

- NFCT-07-Barre d'avancement

Utilisabilité -> compréhension, opérabilité

- NFCT-08-Versions Androïd

Portabilité -> adaptabilité
C'est mieux de préciser la version et les marques

- NFCT-09-Pop-up suppression

Fonctionnalité -> sécurité
Fiabilité

- NFCT-10-Logs

Maintenabilité -> analyse

- NFCT-11-Performance serveur

Efficacité -> Resource

- NFCT-12-Planification production

Maintenabilité -> changeabilité

- NFCT-13-Aide en Ligne

Utilisabilité -> compréhension

- NFCT-14-Arrêt de traitement

Utilisabilité -> opérabilité

- NFCT-15-Coupure liaison entrepots

Fiabilité -> tolérance aux pannes

- NFCT-16-Modification modèles

Maintenabilité -> Changeabilité

- NFCT-17-Changement design

Usabilité -> attractivité

- NFCT-18-Erreur planification

Fiabilité -> maturité

- NFCT-19-Recherche d'article

Efficacité -> temps

- NFCT-20-Partage serveur

Portabilité -> cohéxistance

- NFCT-21-Système d'exploitation

Portabilité -> adaptabilité

- NFCT-22-Qualité des développements

Maintenabilité -> compliance
Interne
Là on veut que si certain design patterns soient à la mode bah on les utilise. On précisé pas lesquels du coup c'est vague.

- NFCT-23-Evolution de l'application

Miaintenabilité -> analyse
Doit préciser le standard, le document technique