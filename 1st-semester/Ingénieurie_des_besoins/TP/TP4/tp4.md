# Scénario 3.2.1. : Suivre l'obtention d'un stage
1. L'étudiant transmet la fiche de stage remplie au responsable de stage
2. Le responsable de stage valide le stage
3. L'étudiant transmet la convention de stage remplie au responsable de stage
4. Le responsable de stage vérifie la convention
5. Le responsable de stage signe la convention
6. Le responsable de stage transmet la convention à la direction de l'UFR
7. La direction de l'UFR signe la convention
8. Le responsable des stages garde un exemplaire de tous les documents
9. Le responsable des stages transmet les autres exemplaires à la secrétaire de la formation
10. La secrétaire de la formation transmet un exemplaire à l'étudiant et à l'entreprise associée

Variante 1 (métier alternatif)
6.1. Le stage est à l'étranger : Le responsable de stage transmet la convention au directeur de la DEVE
7.1. Le directeur de la DEVE signe la convention
goto 8.

Extension 2 (métier exceptionnel)
7.2. La convention est perdue : La direction de l'UFR obtient à nouveau une copie de la convention
8.2. La direction de l'UFR signe la convention
goto 8.