Est-ce qu'un ou plusieurs diagramme(s) de cas d'utilisation serai(en)t adequat(s) ? Pourquoi ? Dans le cas d'une réponse positive, construire le ou les diagrammes qui vous semblent approprié(s).

Faire un diagramme de classe pour l'élaboration d'une proposition client

`Un Use Case est bien quand on a des actions sans liens temporels entre elles. Si on nous dit plutot de faire l'un ouis l'autre puis l'autre on passe par un diagramme d'activité`

Est-ce qu'un ou plusieurs diagramme(s) de classe serai(en)t adequat(s) ? Pourquoi ? Dans le cas d'une réponse positive, établir le ou les diagrammes qui vous semblent approprié(s).

Est-ce qu'un ou plusieurs diagramme(s) d'activité serai(en)t adequat(s) ? Pourquoi ? Dans le cas d'une réponse positive, réaliser le ou les diagrammes qui vous semblent approprié(s).

Est-ce qu'un ou plusieurs diagramme(s) d'état-transition serai(en)t adequat(s) ? Pourquoi ? Dans le cas d'une réponse positive, donner le ou les diagrammes qui vous semblent approprié(s).

Est-ce qu'un ou plusieurs autres modèles de la notation UML serai(en)t adequat(s) ? Pourquoi ? Dans le cas d'une réponse positive, faire le ou les diagrammes qui vous semblent approprié(s).

Si vous connaissez d'autres modèles ou notations qui vous semblent intéressants pour vous aider dans le processus d'analyse des besoins, indiquez-les, précisez en quoi il sont appropriés et réaliser les diagrammes correspondant.