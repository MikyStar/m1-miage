window.onload = init;

function init()
{
	let firebaseApp = firebase.initializeApp(
		{
			apiKey: "AIzaSyBplpaS6g5C2hjB1Kv_sdronUkKUl4qJPc",
			authDomain: "m1-miage-web-tp1.firebaseapp.com",
			databaseURL: "https://m1-miage-web-tp1.firebaseio.com",
			projectId: "m1-miage-web-tp1",
			storageBucket: "m1-miage-web-tp1.appspot.com",
			messagingSenderId: "588705902854"
		}
	);

	let db = firebaseApp.database();

	let restaurantsRef = db.ref( 'restaurants' );

	new Vue(
	{
		el: "#app",

		data:
		{
			nom: "",
			cuisine: ""
		},

		firebase:
		{
			restaurants: restaurantsRef
		},

		methods:
		{
			supprimerRestaurant : function ( index )
			{
				restaurantsRef.child( index['.key'] ).remove();
			},
			ajouterRestaurant : function ()
			{
				let nouveauRestaurant =
				{
					nom: this.nom,
					cuisine: this.cuisine,
					id: _.uniqueId( "restaurant_" )
				}

				restaurantsRef.push( nouveauRestaurant );

				// on remet à zéro les champs
				this.nom = "";
				this.cuisine = "";
			},

			getRowBackgroundColor(index)
			{
				return ( index % 2 ) ? "white" : "cyan";
			}
		}
	} );
}